package com.h2ss.cat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class CatPortalServiceApplication extends
		SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(CatPortalServiceApplication.class, args);
	}

}
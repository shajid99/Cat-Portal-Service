package com.h2ss.cat.controller;

import com.h2ss.cat.entity.Data;
import com.h2ss.cat.entity.RuleResponse;
import com.h2ss.cat.entity.Publication;
import com.h2ss.cat.entity.RedirectRule;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Produces;

@RestController
@CrossOrigin
//@CrossOrigin(origins = { "${portal.client.cross.origin}" })
public class RedirectController {

    @GetMapping
    @RequestMapping("/publications")
    @Produces(MediaType.APPLICATION_JSON_VALUE)
    public List publications() {
        List<Publication> publications = new ArrayList<Publication>();
        publications.add(new Publication("CZ", "Czech Republic"));
        publications.add(new Publication("DE", "Germany"));
        publications.add(new Publication("DK", "Denmark"));
        publications.add(new Publication("ES", "Spain"));
        publications.add(new Publication("FR", "France"));
        publications.add(new Publication("GB", "United Kingdom"));
        publications.add(new Publication("HU", "Hungary"));
        publications.add(new Publication("IT", "Italy"));
        publications.add(new Publication("PL", "Poland"));
        publications.add(new Publication("XR", "CROSS ENGLISH"));
        publications.add(new Publication("SA", "Saj"));

        return publications;
    }

    @RequestMapping(value = "/saveRedirect", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity <Object> saveRedirect(@RequestBody RedirectRule redirectRule) {
        System.out.println("CategoryID = " + redirectRule.getCategoryID());
        System.out.println("MarketID = " + redirectRule.getMarketID());
        System.out.println("NewUrl = " + redirectRule.getNewUrl());
        System.out.println("OldUrl = " + redirectRule.getOldUrl());

        RuleResponse ruleResponse;
        if (! StringUtils.isEmpty(redirectRule.getCategoryID()) &&
                ! StringUtils.isEmpty(redirectRule.getMarketID()) &&
                ! StringUtils.isEmpty(redirectRule.getNewUrl()) &&
                ! StringUtils.isEmpty(redirectRule.getOldUrl())) {
            ruleResponse = new RuleResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), new Data(redirectRule.getCategoryID()));
            return new ResponseEntity<Object>(ruleResponse, HttpStatus.OK);
        }
        else {
            ruleResponse = new RuleResponse(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND.name(), new Data());
            return new ResponseEntity<Object>(ruleResponse, HttpStatus.NOT_FOUND);
        }
    }




//
//      'POST /saveRedirect': (req, res) => {
//    const { marketID, categoryID, oldUrl, newUrl } = req.body;
//        if (marketID !== '' &&
//                categoryID !== '' &&
//                oldUrl !== '' &&
//                newUrl !== '') { // other validation required
//            return res.json({
//                    status: 'ok',
//                    data: {
//                id: categoryID,
//            }
//      });
//        } else {
//            return res.status(403).json({
//                    status: 'error',
//                    code: 403
//      });
//        }
//    },


}
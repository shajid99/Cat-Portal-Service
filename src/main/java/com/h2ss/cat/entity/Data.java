package com.h2ss.cat.entity;

public class Data {
    private String categoryID;

    public Data() {
        categoryID = "";
    }

    public Data(String categoryID) {
        this.categoryID = categoryID;
    }

    public String getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }
}

package com.h2ss.cat.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat
public class Publication {

    private String value;
    private String label;

    public Publication() {
    }

    public Publication(String value, String label) {
        this.value = value;
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public String getLabel() {
        return label;
    }
}
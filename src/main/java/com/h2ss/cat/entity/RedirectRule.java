package com.h2ss.cat.entity;

public class RedirectRule {
    private String marketID;
    private String categoryID;
    private String oldUrl;
    private String newUrl;

    public RedirectRule() {
    }

    public RedirectRule(String marketID, String categoryID, String oldUrl, String newUrl) {
        this.marketID = marketID;
        this.categoryID = categoryID;
        this.oldUrl = oldUrl;
        this.newUrl = newUrl;
    }

    public String getMarketID() {
        return marketID;
    }

    public void setMarketID(String marketID) {
        this.marketID = marketID;
    }

    public String getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }

    public String getOldUrl() {
        return oldUrl;
    }

    public void setOldUrl(String oldUrl) {
        this.oldUrl = oldUrl;
    }

    public String getNewUrl() {
        return newUrl;
    }

    public void setNewUrl(String newUrl) {
        this.newUrl = newUrl;
    }
}

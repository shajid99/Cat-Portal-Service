package com.h2ss.cat.entity;

public class RuleResponse {
    private int code;
    private String status;
    private Data data;

    public RuleResponse(int code, String status, Data data) {
        this.code = code;
        this.status = status;
        this.data = data;
    }

    public RuleResponse() {
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
